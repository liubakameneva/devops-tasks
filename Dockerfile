FROM python:3.8-slim

WORKDIR /opt/clinic

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY clinic/ ./

COPY docker-entrypoint.sh/ ./
ENTRYPOINT ["./docker-entrypoint.sh"]

CMD ["gunicorn", "clinic.wsgi"]

